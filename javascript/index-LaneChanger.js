var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function () {
//  return send({
//    msgType: "join",
//    data: {
//      name: botName,
//      key: botKey
//    }
//  });

    return send({
        msgType: "createRace",
        data: {
            botId: {
                name: botName,
                key: botKey
            },
            trackName: 'keimola'
        }
    });
});


function keepOnLane(keepOnLineIndex) {
    var turnDirection;

    if (keepOnLineIndex < sendLaneIndex) {
        turnDirection = 'Left';
    } else if (keepOnLineIndex > sendLaneIndex) {
        turnDirection = 'Right';
    }

    console.log('============================================================ ANULOWANIE ZMIANY PASA');

    send({
        msgType: "switchLane",
        data: turnDirection
    });

    sendLaneIndex = keepOnLineIndex;

    laneChanged = true;
}

function changeLane(index) {
    var turnDirection,
        theSameLine = false;

    if (currentLaneIndex < index) {
        turnDirection = 'Right';
    } else if (currentLaneIndex > index) {
        turnDirection = 'Left';
    } else {
        theSameLine = true;
    }

    if (!theSameLine) {
        console.log('============================================================ ZMIANA PASA');

        send({
            msgType: "switchLane",
            data: turnDirection
        });

        sendLaneIndex = index;

        laneChanged = true;
    }
}

function getIndexOfNextAngle(track, currentPieceIndex) {
    var i, len = track.pieces.length;

    for (i = currentPieceIndex; i < len; i++) {
        if (typeof track.pieces[i].angle !== 'undefined') {
            return i;
        }
    }

    return null;
}

function getIndexOfNextSwitch(track, index) {
    var i, len = track.pieces.length;

    for (i = index; i < len; i++) {
        if (typeof track.pieces[i].switch !== 'undefined') {
            return i;
        }
    }

    return null;
}

function getOnShortestLaneBetweenSwitches(track, currentPieceIndex) {
    var nextSwitchIndex = getIndexOfNextSwitch(track, currentPieceIndex),
        afterNextSwitchIndex,
        newshortestLaneIndex;

    if (nextSwitchIndex !== null) {
        afterNextSwitchIndex = getIndexOfNextSwitch(track, nextSwitchIndex + 1);

        if (afterNextSwitchIndex !== null) {
            newshortestLaneIndex = calculateRoute(nextSwitchIndex, afterNextSwitchIndex);

            return newshortestLaneIndex;
        }
    }

    return null;
}

function calculateRoute(from, to) {
    var i = 0, j = 0,
        absAngle, lanes = [],
        shortestRoute, shortestLaneIndex;

    if (typeof from === 'number' && typeof to === 'number') {
        for (i; i < numberOfLanes; i++) {
            lanes[i] = 0;
        }

        for (i = 0; i < numberOfLanes; i++) {
            for (j = from; j <= to; j++) {
                if (typeof track.pieces[j].length !== 'undefined') {
                    // prosta
                    lanes[i] += track.pieces[j].length;
                }
                if (typeof track.pieces[j].angle !== 'undefined') {
                    // zakręt
                    absAngle = Math.abs(track.pieces[j].angle);
                    if (track.pieces[j].angle > 0) {
                        // zakręt w prawo
                        lanes[i] += (absAngle / 360) * 2 * 3.14 * (track.pieces[j].radius - i * 20);
                    } else {
                        // zakręt w lewo
                        lanes[i] += (absAngle / 360) * 2 * 3.14 * (track.pieces[j].radius + i * 20);
                    }
                }
            }

            if (i === 0) {
                shortestLaneIndex = 0;
                shortestRoute = lanes[0];
            } else if (lanes[i] < shortestRoute) {
                shortestRoute = lanes[i];
                shortestLaneIndex = i;
            }
        }

        return shortestLaneIndex;
    }

    return null;
}

function getOnShortestLane(track, currentPieceIndex) {
    /**
     * poniższe zmiany zakładają dwa tory (patrz returny)
     * TODO: dostosować do wielu linii
     */
    var index = getIndexOfNextAngle(track, currentPieceIndex),
        nextPiece, afterNextPiece;

    if (index !== null) {
        /**
         * null oznacza, że nie ma kolejnego zakrętu. w sumie nie będzie trzeba wtedy też zmieniąc pasu
         * chyba, że będziemy chcieli komuś zajechać drogę... ale to później
         */

        nextPiece = track.pieces[index],
        afterNextPiece = track.pieces[index + 1];

        /*console.log('nextPiece', nextPiece);
        console.log('afterNextPiece', afterNextPiece);*/

        if (nextPiece.angle > 0) {
            // z perspektywy samochodu jest to zakręt w prawo

            if (typeof afterNextPiece.angle !== 'undefined' && typeof nextPiece.angle !== 'undefined') {
                // samochód znajduje się przed łukiem lub na długim łuku

                if (typeof track.pieces[currentPieceIndex].angle === 'undefined') {
                    // samochód znajduje się przed łukiem, jest na prostej
                    return 1;
                } else {
                    // jest na zakręcie
                    return 0;
                }
            } else {
                // samochód wychodzi z zakrętu
                return 1;
            }

        } else if (nextPiece.angle < 0) {
            // z perspektywy samochodu jest to zakręt w lewo

            if (typeof afterNextPiece.angle !== 'undefined' && typeof nextPiece.angle !== 'undefined') {
                // samochód znajduje się przed łukiem lub na długim łuku

                if (typeof track.pieces[currentPieceIndex].angle === 'undefined') {
                    // samochód znajduje się przed łukiem, jest na prostej
                    // wciąż na prostej
                    return 0;
                } else {
                    // jest na zakręcie
                    return 1;
                }
            } else {
                // samochód wychodzi z zakrętu
                return 1;
            }
        }
    }

    // jeśli nic nie udało się wcześniej zwrócić to znaczy, że obecny tor jest najlepszy
    return currentLaneIndex;
}

function send(json) {
    client.write(JSON.stringify(json));
    return client.write('\n');
}

jsonStream = client.pipe(JSONStream.parse());

function pushTurbo() {
    send({"msgType": "turbo", "data": "Pow pow pow pow pow, or your of personalized turbo message"});
}

function pushThrottle(thro) {
    send({
        msgType: "throttle",
        data: thro
    });
}

function checkBigAngle(chunk) {
    if (Math.abs(chunk.angle) >= 45 && Math.abs(chunk.radius) <= 100) { // ostry zakret w nastepnym czanku
        return true;
    } else {
        return false;
    }
}
function checkDistanceToEnd(distance, chunk, diff) {
    if (distance > chunk.length - diff) { // koncowka obecnego czanka
        return true;
    } else {
        return false;
    }
}

var currentThrottle = 1,
    lastAngle,
    angleDiff,
    absAngle,
    anglePlus,
    diffPlus,
    track,
    lastDistance,
    avrgSpeed = 0,
    avrgSpeedSum = 0,
    lastSpeed = 0,
    ticks = 0,
    maxAngle,
    currentLap = 0,
    maxLaps = 0,
    currentLaneIndex = 0,
    numberOfLanes,
    isTurboAv = false,
    /*  Oznaczenie, czy prośba o zmianę toru została wysłana. Blokuje to ciągłe nadawanie o zmianę toru. Nie można na raz wysyłać Trottle i switchLane*/
    laneChanged = false,
    // Liczba okrążeń
    laps,
    // Index najkrótszego toru (w najbliższej perspektywie)
    shortestLaneIndex,
    shortestLaneBetweenSwitchesIndex,
    // Index toru, który ma zostać ustawiony na naszą prośbę (wysłany, ale jeszcze nie zmieniony)
    sendLaneIndex;

jsonStream.on('data', function (data) {
    var angle, currentPiece, currentPieceIndex, piece, nextPiece, absDiff, distance, speed, distanceToBreak;
    if (data.msgType === 'carPositions') {
        angle = data.data[0].angle;
        angleDiff = angle - lastAngle;
        absAngle = Math.abs(angle);
        absDiff = Math.abs(angleDiff);
        diffPlus = true;
        piece = data.data[0].piecePosition;
        currentPieceIndex = piece.pieceIndex;
        currentPiece = track.pieces[currentPieceIndex];
        nextPiece = track.pieces[currentPieceIndex + 1];
        farPiece = track.pieces[currentPieceIndex + 2];
        distance = data.data[0].piecePosition.inPieceDistance;
        speed = Math.round((distance - lastDistance) * 100) / 100;
        ticks += 1;
        laps = data.data[0].piecePosition.lap;
        avrgSpeedSum = avrgSpeedSum + lastSpeed;
        avrgSpeed = Math.floor(avrgSpeedSum / ticks);
        maxAngle = (lastAngle > angle) ? lastAngle : maxAngle;
        shortestLaneIndex = getOnShortestLane(track, currentPieceIndex);
        shortestLaneBetweenSwitchesIndex = getOnShortestLaneBetweenSwitches(track, currentPieceIndex);

        /* LOGIC HERE */
        if (angle > 0) {
            anglePlus = true;
        } else if (angle < 0) {
            anglePlus = false;
        } else {
        }

        if (angleDiff > 0) {
            diffPlus = true;
        } else if (angleDiff < 0) {
            diffPlus = false;
        } else {
        }

        if (absAngle > 10 && absDiff > .8 && anglePlus === diffPlus) { // slide begins
            currentThrottle = 0.3
            if (absDiff > 2) {
                currentThrottle = 0.2;
            }
        } else {
            currentThrottle = 1
        }

        if (typeof nextPiece === 'undefined') { // ostatnia prosta
            currentThrottle = 1;
        } else {
            if (!laneChanged && shortestLaneBetweenSwitchesIndex !== currentLaneIndex) {
                // wysłanie żądania o zmianę toru - aby trasa była krótsza
                changeLane(shortestLaneBetweenSwitchesIndex);
            }

            if (typeof sendLaneIndex !== 'undefined' && typeof currentPiece.switch === 'undefined') {
                if (sendLaneIndex !== shortestLaneBetweenSwitchesIndex && shortestLaneBetweenSwitchesIndex === currentLaneIndex) {
                    /* anulowanie żądania zmiany toru
                     *   tor nie zdążył być zmieniony a już okazało się, że inny tor (lub ten na którym jedziemy)
                     *   jest szybszy
                     *   może się tak stać gdy nie było okazji do zmiany toru
                     * */
                    keepOnLane(currentLaneIndex);
                    laneChanged = false;
                }
            }

            if (typeof nextPiece.angle === 'undefined') { // nie ma zakretu, pelna rura
                currentThrottle = 1;
            } else { // jest zakret
                if (checkBigAngle(nextPiece)) { // ostry zakret w nastepnym czanku
                    if (speed > 9) {
                        distanceToBreak = currentPiece.length;
                    } else if (speed > 8) {
                        distanceToBreak = currentPiece.length - 30;
                    } else {
                        distanceToBreak = (currentPiece.length / 2) - 20
                    }
                    if (checkDistanceToEnd(distance, currentPiece, distanceToBreak)) { // koncowka obecnego czanka
                        console.log('ZARAZ ZAKRET HAMUJ!!!');
                        if (speed > 6.9) {
                            currentThrottle = 0;
                        } else {
                            currentThrottle = 1;
                        }
                    }
                }

                if (typeof farPiece !== 'undefined') {
                    if (checkBigAngle(farPiece)) {
                        if (checkDistanceToEnd(distance, currentPiece, 10)) {
                            console.log('dohamowanie');
                            currentThrottle = 0;
                        }
                    }
                }
            }

            // Jesli na ostrym zakrecie
            if (checkBigAngle(currentPiece)) {
                if ((diffPlus === anglePlus && absDiff > 1.4) || absDiff > 3) {
                    console.log('POSLIZG');
                    if (speed < 6.9) {
                        currentThrottle = 1;
                    } else {
                        currentThrottle = 0;
                    }
                    if (absDiff < 1.3) {
                        currentThrottle = 1;
                    }
                } else {
                    console.log('DAMY RADE CISNIJ!');
                    currentThrottle = 1;
                }
            }
        }

        // zabezpieczenie antyposlizgowe
        if (absAngle > 25 && absDiff > 2.6 && diffPlus === anglePlus || (absAngle > 50 && absDiff > 0.6) || absDiff > 4) {
            if (diffPlus !== anglePlus) { // wychodzimy z poslizgu to pelna rura
                currentThrottle = 1;
            } else {
                console.log('ESP');
                currentThrottle = 0;
            }
            if (absAngle > 55) { // awaryjne hamowanie
                currentThrottle = 0;
            }
        }

        if (absAngle > 59) { // awaryjne hamowanie
            currentThrottle = 0;
        }

        // wychodzimy na prosta - RURA!
        if (typeof nextPiece !== 'undefined') {
            if (typeof nextPiece.angle === 'undefined') {
                if (checkDistanceToEnd(distance, currentPiece, 50)) {
                    console.log("WYCHODZIMY NA PROSTĄ, RURA!!");
                    currentThrottle = 1;
                }
            }
        }

        if (currentPieceIndex === 35 && currentLap+1 === maxLaps) { //35 finland
            send({"msgType": "turbo", "data": "Pow pow pow pow pow, or your of personalized turbo message"});
            //isTurboAv = false;
        }
//        console.log('currentPieceIndex', currentPieceIndex);
        console.log('tr', currentThrottle);
        console.log('speed', speed);
        console.log('angle', angle);
        console.log('maxAngle', maxAngle);
        console.log('diff', angleDiff);
        console.log('distance', data.data[0].piecePosition.inPieceDistance);
        console.log('pieceIndex', currentPieceIndex);
        if (checkBigAngle(currentPiece)) {
            console.log('SPORY ZAKRĘT!');
        }
        if (typeof currentPiece.angle !== 'undefined') {
            console.log('angle', currentPiece.angle);
            console.log('radius', currentPiece.radius);
        } else {
        }
        console.log('====================== ' + data.gameTick + ' ' + laps);

        lastSpeed = speed;
        lastAngle = angle;
        lastDistance = distance;
        currentLap = data.data[0].piecePosition.lap;
        pushThrottle(currentThrottle);
    } else {
        if (data.msgType === 'join') {
            console.log('Joined')
        } else if (data.msgType === 'turboAvailable') {
            isTurboAv = true;
            console.log('********************************************* ****** TURBO!!!!!!!', data.data.turboDurationMilliseconds);
        } else if (data.msgType === 'gameStart') {
            laneChanged = false;

            console.log(data);
            console.log('Race started');
        } else if (data.msgType === 'gameEnd') {
            console.log('Race ended');
            console.log('BestLap', data.data.bestLaps[0].result.millis / 1000);
        } else if (data.msgType === 'gameInit') {
            track = data.data.race.track;
            maxLaps = data.data.race.raceSession.laps;
            numberOfLanes = data.data.race.track.lanes.length;
            console.log(data.data.race.track);
        }

        send({
            msgType: "ping",
            data: {}
        });
    }
});

jsonStream.on('error', function () {
    return console.log("disconnected");
});