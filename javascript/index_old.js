var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function () {
    return send({
        msgType: "join",
        data: {
            name: botName,
            key: botKey
        }
    });

//    return send({
//        msgType: "createRace",
//        data: {
//            botId:{
//                name: botName,
//                key: botKey
//            },
//            trackName: 'germany'
////            trackName: 'keimola'
////            trackName: 'france'
//        }
//    });
});


function keepOnLane(keepOnLineIndex) {
    var turnDirection;

    if (keepOnLineIndex < sendLaneIndex) {
        turnDirection = 'Left';
    } else if (keepOnLineIndex > sendLaneIndex) {
        turnDirection = 'Right';
    }

    console.log('============================================================ ANULOWANIE ZMIANY PASA');

    send({
        msgType: "switchLane",
        data: turnDirection
    });

    sendLaneIndex = keepOnLineIndex;

    laneChanged = true;
}

function changeLane(index) {
    var turnDirection,
        theSameLine = false;

    if (currentLaneIndex < index) {
        turnDirection = 'Right';
    } else if (currentLaneIndex > index) {
        turnDirection = 'Left';
    } else {
        theSameLine = true;
    }

    if (!theSameLine) {
        console.log('============================================================ ZMIANA PASA');

        send({
            msgType: "switchLane",
            data: turnDirection
        });

        sendLaneIndex = index;

        laneChanged = true;
    }
}

function getIndexOfNextAngle(track, currentPieceIndex) {
    var i, len = track.pieces.length;

    for (i = currentPieceIndex; i < len; i++) {
        if (typeof track.pieces[i].angle !== 'undefined') {
            return i;
        }
    }

    return null;
}

function getIndexOfNextSwitch(track, index) {
    var i, len = track.pieces.length;

    for (i = index; i < len; i++) {
        if (typeof track.pieces[i].switch !== 'undefined') {
            return i;
        }
    }

    return null;
}

function getOnShortestLaneBetweenSwitches(track, currentPieceIndex) {
    var nextSwitchIndex = getIndexOfNextSwitch(track, currentPieceIndex),
        afterNextSwitchIndex,
        newshortestLaneIndex;

    if (nextSwitchIndex !== null) {
        afterNextSwitchIndex = getIndexOfNextSwitch(track, nextSwitchIndex + 1);

        if (afterNextSwitchIndex !== null) {
            newshortestLaneIndex = calculateRoute(nextSwitchIndex, afterNextSwitchIndex);

            return newshortestLaneIndex;
        }
    }

    return null;
}

function calculateRoute(from, to) {
    var i = 0, j = 0,
        absAngle, lanes = [],
        shortestRoute, shortestLaneIndex;

    if (typeof from === 'number' && typeof to === 'number') {
        for (i; i < numberOfLanes; i++) {
            lanes[i] = 0;
        }

        for (i = 0; i < numberOfLanes; i++) {
            for (j = from; j <= to; j++) {
                if (typeof track.pieces[j].length !== 'undefined') {
                    // prosta
                    lanes[i] += track.pieces[j].length;
                }
                if (typeof track.pieces[j].angle !== 'undefined') {
                    // zakręt
                    absAngle = Math.abs(track.pieces[j].angle);
                    if (track.pieces[j].angle > 0) {
                        // zakręt w prawo
                        lanes[i] += (absAngle / 360) * 2 * 3.14 * (track.pieces[j].radius - i * 20);
                    } else {
                        // zakręt w lewo
                        lanes[i] += (absAngle / 360) * 2 * 3.14 * (track.pieces[j].radius + i * 20);
                    }
                }
            }

            if (i === 0) {
                shortestLaneIndex = 0;
                shortestRoute = lanes[0];
            } else if (lanes[i] < shortestRoute) {
                shortestRoute = lanes[i];
                shortestLaneIndex = i;
            }
        }

        return shortestLaneIndex;
    }

    return null;
}

function send(json) {
    client.write(JSON.stringify(json));
    return client.write('\n');
}

jsonStream = client.pipe(JSONStream.parse());

function pushThrottle(thro) {
    send({
        msgType: "throttle",
        data: thro
    });
}

function checkBigAngle(chunk) {
    if (Math.abs(chunk.angle) >= 45 && Math.abs(chunk.radius) <= 100) { // ostry zakret w nastepnym czanku
        return true;
    } else {
        return false;
    }
}
function checkDistanceToEnd(distance, chunk, diff) {
    if (distance > chunk.length - diff) { // koncowka obecnego czanka
        return true;
    } else {
        return false;
    }
}

function maxSpeedForChunk(chunk) {
    if (typeof chunk !== 'undefined') {
        if (typeof chunk.radius !== 'undefined') {
            return chunk.radius / 14.2; // 14.4
        }
    }
    return 10;
}

/*
 8 dl - .16 speeda
 */
function breakDistance(currentSpeed, expectedSpeed) {
    var diffSpeed = currentSpeed - expectedSpeed;

    return (diffSpeed / 0.16) * 8;
}

function calculateDistanceForChunk(chunk) {
    if (typeof chunk === 'undefined') {
        return 0;
    }
    if (typeof chunk.radius !== 'undefined') {
        return Math.abs((chunk.angle / 360) * 2 * Math.PI * chunk.radius);
    } else if (typeof chunk.length !== 'undefined') {
        return chunk.length;
    }
}

function computeDistanceForChunks(chunks) {
    var distance = 0;
    for (var i = 0; i < chunks.length; i++) {
        distance += calculateDistanceForChunk(chunks[i]);
    }
    return distance;
}

function getCarIndex(dataList) {
    for (var i = 0; i < dataList.length; i += 1) {
        if (dataList[i].id.name === botName) {
            return i;
        }
    }

    throw new Error('Cannot get car Index ;(');
}

var currentThrottle = 1,
    lastAngle,
    angleDiff,
    absAngle,
    anglePlus,
    diffPlus,
    track,
    lastDistance,
    avrgSpeed = 0,
    avrgSpeedSum = 0,
    lastSpeed = 0,
    ticks = 0,
    maxAngle,
    currentLaneIndex = 0,
    farPiece,
    farx2Piece,
    farx3Piece,
    maxLaps = 0,
    currentLap = 0,
    numberOfLanes,
    isTurboAv = false,
    carIndex,
    laneChanged = false, /*  Oznaczenie, czy prośba o zmianę toru została wysłana. Blokuje to ciągłe nadawanie o zmianę toru
 Nie można na raz wysyłać Trottle i switchLane*/
    laps,                       // Liczba okrążeń
    shortestLaneBetweenSwitchesIndex,
    sendLaneIndex;              // Index toru, który ma zostać ustawiony na naszą prośbę (wysłany, ale jeszcze nie zmieniony)

jsonStream.on('data', function (data) {
    var angle, currentPiece, currentPieceIndex, piece, nextPiece, absDiff, distance, speed, distanceToBreak;

    if (data.msgType === 'carPositions') {
        carIndex = getCarIndex(data.data);
        angle = Math.abs(data.data[carIndex].angle);
        angleDiff = angle - lastAngle;
        absAngle = Math.abs(angle);
        absDiff = Math.abs(angleDiff);
        diffPlus = true;
        piece = data.data[carIndex].piecePosition;
        currentPieceIndex = piece.pieceIndex;
        currentPiece = track.pieces[currentPieceIndex];
        nextPiece = track.pieces[currentPieceIndex + 1];
        farPiece = track.pieces[currentPieceIndex + 2];
        farx2Piece = track.pieces[currentPieceIndex + 3];
        farx3Piece = track.pieces[currentPieceIndex + 4];
        distance = data.data[carIndex].piecePosition.inPieceDistance;
        speed = Math.round((distance - lastDistance) * 100) / 100;
        ticks += 1;
        avrgSpeedSum = avrgSpeedSum + lastSpeed;
        avrgSpeed = Math.floor(avrgSpeedSum / ticks);
        maxAngle = (lastAngle > absAngle) ? lastAngle : maxAngle;
        shortestLaneBetweenSwitchesIndex = getOnShortestLaneBetweenSwitches(track, currentPieceIndex);
        currentThrottle = 1;

        /* LOGIC HERE */
        if (angle > 0) {
            anglePlus = true;
        } else if (angle < 0) {
            anglePlus = false;
        } else {
        }

        if (angleDiff > 0) {
            diffPlus = true;
        } else if (angleDiff < 0) {
            diffPlus = false;
        } else {
        }

        if (absAngle > 10 && absDiff > .8 && anglePlus === diffPlus) { // slide begins
            currentThrottle = 0.3
            if (absDiff > 2) {
                currentThrottle = 0.2;
            }
        } else {
            currentThrottle = 1
        }

        if (typeof nextPiece === 'undefined') { // ostatnia prosta
            currentThrottle = 1;
        } else {
            if (!laneChanged && shortestLaneBetweenSwitchesIndex !== currentLaneIndex) {
                // wysłanie żądania o zmianę toru - aby trasa była krótsza
                changeLane(shortestLaneBetweenSwitchesIndex);
            }

            if (typeof sendLaneIndex !== 'undefined' && typeof currentPiece.switch === 'undefined') {
                if (sendLaneIndex !== shortestLaneBetweenSwitchesIndex && shortestLaneBetweenSwitchesIndex === currentLaneIndex) {
                    /* anulowanie żądania zmiany toru
                     *   tor nie zdążył być zmieniony a już okazało się, że inny tor (lub ten na którym jedziemy)
                     *   jest szybszy
                     *   może się tak stać gdy nie było okazji do zmiany toru
                     * */
                    keepOnLane(currentLaneIndex);
                    laneChanged = false;
                }
            }

            if (typeof nextPiece.angle === 'undefined') { // nie ma zakretu, pelna rura
                currentThrottle = 1;
            } else { // jest zakret
                if (checkBigAngle(nextPiece)) { // ostry zakret w nastepnym czanku
                    if (speed > 9) {
                        distanceToBreak = currentPiece.length;
                    } else if (speed > 8) {
                        distanceToBreak = currentPiece.length - 30;
                    } else {
                        distanceToBreak = (currentPiece.length / 2) - 20
                    }
                    if (checkDistanceToEnd(distance, currentPiece, distanceToBreak)) { // koncowka obecnego czanka
                        console.log('ZARAZ ZAKRET HAMUJ!!! max speed: ', maxSpeedForChunk(nextPiece));
                        if (speed > maxSpeedForChunk(nextPiece)) {
                            currentThrottle = 0;
                        } else {
                            currentThrottle = 1;
                        }
                    }
                }
                var diff;

                console.log('MAX SPEED DLA NASTEPNEGO', maxSpeedForChunk(nextPiece));
            }

            if (breakDistance(speed, maxSpeedForChunk(farPiece)) > computeDistanceForChunks([nextPiece])) {
                currentThrottle = 0;
            }

            // Jesli na ostrym zakrecie
            if (checkBigAngle(currentPiece)) {
                if ((diffPlus === anglePlus && absDiff > 1.4) || absDiff > 3) {
                    console.log('POSLIZG, max speed: ', maxSpeedForChunk(currentPiece));
                    if (speed < maxSpeedForChunk(currentPiece)) {
                        currentThrottle = 1;
                    } else {
                        currentThrottle = 0;
                    }
                    if (absDiff < 1.3) {
                        currentThrottle = 1;
                    }
                } else {
                    console.log('DAMY RADE CISNIJ!');
                    currentThrottle = 1;
                }
            }
        }

        // zabezpieczenie antyposlizgowe
        if ((absAngle > 25 && absDiff > 2.6 && diffPlus === anglePlus) || (absAngle > 50 && absDiff > 0.6) || absDiff > 4) {
            if (diffPlus !== anglePlus) { // wychodzimy z poslizgu to pelna rura
                currentThrottle = 1;
                if (absDiff > 3.5) {
                    currentThrottle = 0;
                }
            } else {
                console.log('ESP');
                currentThrottle = 0;
            }
            if (absAngle > 55) { // awaryjne hamowanie
                currentThrottle = 0;
            }
        }

        if (absAngle > 59) { // awaryjne hamowanie
            currentThrottle = 0;
        }

        // wychodzimy na prosta - RURA!
        if (typeof nextPiece !== 'undefined') {
            if (typeof nextPiece.angle === 'undefined' && typeof currentPiece.angle !== 'undefined') {
                if (checkDistanceToEnd(distance, currentPiece, 20)) {
                    console.log("WYCHODZIMY NA PROSTĄ, RURA!!");
                    currentThrottle = 1;
                }
            }
        }


        if (currentPieceIndex >= 35 && currentLap + 1 === maxLaps) { //35 finland
            send({"msgType": "turbo", "data": "Pow pow pow pow pow, or your of personalized turbo message"});
            //isTurboAv = false;
        }

        console.log('tr', currentThrottle);
        console.log('speed', speed);
        console.log('angle', Math.round(angle * 100) / 100);
        console.log('diff', Math.round(absDiff * 100) / 100);
        console.log('distance', Math.round(data.data[carIndex].piecePosition.inPieceDistance * 100) / 100);
        console.log('currentLap', currentLap, ' max laps: ', maxLaps);
        console.log('pieceIndex', currentPieceIndex);
        if (checkBigAngle(currentPiece)) {
            console.log('SPORY ZAKRĘT!');
        }
        if (typeof currentPiece.angle !== 'undefined') {
            console.log('angle', currentPiece.angle);
            console.log('radius', currentPiece.radius);
        } else {
        }
        console.log('======================');

        lastSpeed = speed;
        lastAngle = absAngle;
        lastDistance = distance;
        currentLap = data.data[carIndex].piecePosition.lap;
        pushThrottle(currentThrottle);
    } else {
        if (data.msgType === 'join') {
            console.log('Joined')
        } else if (data.msgType === 'turboAvailable') {
            isTurboAv = true;
            console.log('********************************************* ****** TURBO!!!!!!!', data.data.turboDurationMilliseconds);
        } else if (data.msgType === 'gameStart') {
            console.log(data);
            console.log('Race started');
        } else if (data.msgType === 'gameEnd') {
            console.log('Race ended');
        } else if (data.msgType === 'gameInit') {
            track = data.data.race.track;
            console.log(data.data.race.track);
            maxLaps = data.data.race.raceSession.laps;
            numberOfLanes = data.data.race.track.lanes.length;
        }

        send({
            msgType: "ping",
            data: {}
        });
    }
});

jsonStream.on('error', function () {
    return console.log("disconnected");
});