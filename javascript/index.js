var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

var france = [
    { length: 102 },
    { length: 94 },
    { radius: 200, angle: 22.5, switch: true },
    { radius: 200, angle: 22.5 },
    { radius: 200, angle: 22.5 },
    { radius: 200, angle: 22.5 },
    { radius: 100, angle: 45 },
    { radius: 100, angle: 45, switch: true },
    { length: 53 },
    { radius: 200, angle: -22.5 },
    { radius: 50, angle: -45 },
    { radius: 100, angle: 45 },
    { radius: 100, angle: 45 },
    { radius: 100, angle: 45 },
    { radius: 50, angle: -45 },
    { radius: 50, angle: -45 },
    { radius: 50, angle: -45 },
    { radius: 200, angle: -22.5, switch: true },
    { length: 94 },
    { length: 94 },
    { radius: 50, angle: 45 },
    { radius: 50, angle: 45 },
    { radius: 50, angle: 45 },
    { radius: 50, angle: 45 },
    { length: 99, switch: true },
    { length: 99 },
    { radius: 100, angle: -45 },
    { radius: 100, angle: -45 },
    { radius: 100, angle: -45 },
    { radius: 100, angle: 45 },
    { radius: 200, angle: 22.5, switch: true },
    { radius: 200, angle: 22.5 },
    { length: 53 },
    { radius: 200, angle: 22.5 },
    { radius: 200, angle: 22.5 },
    { radius: 100, angle: 45 },
    { radius: 200, angle: 22.5 },
    { radius: 200, angle: 22.5 },
    { length: 94, switch: true },
    { length: 94 },
    { length: 94 },
    { length: 94 },
    { length: 94 },
    { length: 91 }
];

var glob = {};
var gInit = {};


client = net.connect(serverPort, serverHost, function () {
  /*return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });*/

    return send({
        msgType: "createRace",
        data: {
            botId: {
                name: botName,
                key: botKey
            },
            laps: 3,
////            carCount: 2,
////            password: 'asdf',
//            trackName: 'usa'
////            trackName: 'france'
//            trackName: 'germany'
//            trackName: 'keimola'
//            trackName: 'imola'
//            trackName: 'england'
            trackName: 'suzuka'
        }
    });
});

function pushThrottle(thro, gameTick) {
    send({
        msgType: "throttle",
        data: thro,
        gameTick: gameTick
    });
}

function send(json) {
    client.write(JSON.stringify(json));
    return client.write('\n');
}

var jsonStream = client.pipe(JSONStream.parse());

function changeLane(index) {
    var turnDirection,
        theSameLine = false;

    if (glob.currentLaneIndex < index) {
        turnDirection = 'Right';
        glob.sendLaneIndex = glob.currentLaneIndex + 1;
    } else if (glob.currentLaneIndex > index) {
        turnDirection = 'Left';
        glob.sendLaneIndex = glob.currentLaneIndex - 1;
    } else {
        theSameLine = true;
    }

    if (!theSameLine) {
        console.log('============================================================ ZMIANA PASA na ' + index);

        send({
            msgType: "switchLane",
            data: turnDirection
        });

        glob.laneChanged = true;
    }
}

function calculateDistanceToNextAngle (cPosition) {
    var i, sum = 0,
        indexOfNextAngle = getIndexOfNextAngle(gInit.track, cPosition.piecePosition.pieceIndex);

    for (i = cPosition.piecePosition.pieceIndex; i < indexOfNextAngle; i++) {
        if (typeof gInit.track.pieces[i].length !== 'undefined') {
            // prosta
            sum += gInit.track.pieces[i].length;
        }
        if (typeof gInit.track.pieces[i].angle !== 'undefined') {
            // zakręt
            absAngle = Math.abs(gInit.track.pieces[i].angle);
            if (gInit.track.pieces[i].angle > 0) {
                // zakręt w prawo
                sum += (absAngle / 360) * 2 * 3.14 * (gInit.track.pieces[i].radius - gInit.track.lanes[glob.currentLaneIndex].distanceFromCenter);
            } else {
                // zakręt w lewo
                sum += (absAngle / 360) * 2 * 3.14 * (gInit.track.pieces[i].radius + gInit.track.lanes[glob.currentLaneIndex].distanceFromCenter);
            }
        }
    }
    if (typeof gInit.track.pieces[cPosition.piecePosition.pieceIndex].length !== 'undefined') {
        sum += gInit.track.pieces[cPosition.piecePosition.pieceIndex].length - cPosition.inPieceDistance;
    }
    if (typeof gInit.track.pieces[cPosition.piecePosition.pieceIndex].angle !== 'undefined') {
        // zakręt
        absAngle = Math.abs(gInit.track.pieces[cPosition.piecePosition.pieceIndex].angle);
        if (gInit.track.pieces[cPosition.piecePosition.pieceIndex].angle > 0) {
            // zakręt w prawo
            sum += (absAngle / 360) * 2 * 3.14 * (gInit.track.pieces[cPosition.piecePosition.pieceIndex].radius - gInit.track.lanes[glob.currentLaneIndex].distanceFromCenter);
        } else {
            // zakręt w lewo
            sum += (absAngle / 360) * 2 * 3.14 * (gInit.track.pieces[cPosition.piecePosition.pieceIndex].radius + gInit.track.lanes[glob.currentLaneIndex].distanceFromCenter);
        }

        sum -= cPosition.inPieceDistance;
    }

    return sum;
}

function getIndexOfNextAngle(track, currentPieceIndex) {
    var i, len = track.pieces.length;

    for (i = currentPieceIndex; i < len; i++) {
        if (typeof track.pieces[i].angle !== 'undefined') {
            return i;
        }
    }

    return null;
}

function getIndexOfNextSwitch(track, index) {
    var i, len = track.pieces.length;

    for (i = index; i < len; i++) {
        if (typeof track.pieces[i].switch !== 'undefined') {
            return i;
        }
    }

    return null;
}

function getOnShortestLaneBetweenSwitches(track, currentPieceIndex) {
    var nextSwitchIndex = getIndexOfNextSwitch(track, currentPieceIndex),
        afterNextSwitchIndex,
        newshortestLaneIndex;

    if (nextSwitchIndex !== null) {
        afterNextSwitchIndex = getIndexOfNextSwitch(track, nextSwitchIndex + 1);

        if (afterNextSwitchIndex !== null) {
            newshortestLaneIndex = calculateRoute(nextSwitchIndex, afterNextSwitchIndex);

            return newshortestLaneIndex;
        }
    }

    return glob.currentLaneIndex;
}

function calculateRoute(from, to) {
    var i = 0, j = 0,
        absAngle, lanes = [],
        shortestRoute, shortestLaneIndex;

    if (typeof from === 'number' && typeof to === 'number') {
        for (i; i < numberOfLanes; i++) {
            lanes[i] = 0;
        }

//        console.log('Routes from chank ' + from + ' to chank ' + to + ':');
        for (i = 0; i < numberOfLanes; i++) {
            for (j = from; j <= to; j++) {
                if (typeof gInit.track.pieces[j].length !== 'undefined') {
                    // prosta
                    lanes[i] += gInit.track.pieces[j].length;
                }
                if (typeof gInit.track.pieces[j].angle !== 'undefined') {
                    // zakręt
                    absAngle = Math.abs(gInit.track.pieces[j].angle);
                    if (gInit.track.pieces[j].angle > 0) {
                        // zakręt w prawo
                        lanes[i] += (absAngle / 360) * 2 * 3.14 * (gInit.track.pieces[j].radius - gInit.track.lanes[i].distanceFromCenter);
                    } else {
                        // zakręt w lewo
                        lanes[i] += (absAngle / 360) * 2 * 3.14 * (gInit.track.pieces[j].radius + gInit.track.lanes[i].distanceFromCenter);
                    }
                }
            }

            if (i === 0) {
                shortestLaneIndex = 0;
                shortestRoute = lanes[0];
            } else if (lanes[i] < shortestRoute) {
                shortestRoute = lanes[i];
                shortestLaneIndex = i;
            }
//            console.log('lanes[' + i + '] = ' + lanes[i] + ' --- ' + gInit.track.lanes[i].distanceFromCenter);
        }
//        console.log('\n');

        return shortestLaneIndex;
    }

    return null;
}

function calculateDistanceForChunk(chunk, laneIndex) {
    if (typeof chunk === 'undefined') {
        return 0;
    }
    if (typeof chunk.radius !== 'undefined') {
        if (chunk.angle > 0) {
            return Math.abs(chunk.angle / 360) * 2 * Math.PI * (chunk.radius - gInit.track.lanes[laneIndex].distanceFromCenter);
        } else {
            return Math.abs(chunk.angle / 360) * 2 * Math.PI * (chunk.radius + gInit.track.lanes[laneIndex].distanceFromCenter);
        }
    } else if (typeof chunk.length !== 'undefined') {
        return chunk.length;
    }
}

function maxSpeedForChunk(chunk, laneIndex) {

    if (typeof chunk !== 'undefined') {
        if (typeof chunk.radius !== 'undefined') {
            var diff = (chunk.angle > 0) ? - gInit.track.lanes[laneIndex].distanceFromCenter : gInit.track.lanes[laneIndex].distanceFromCenter;
            if (Math.abs(chunk.radius) >= 200) {
                diff += 10;
            }else if (Math.abs(chunk.radius) <= 50) {
                diff -= 5;
            }

            console.log('=================== diff[ ' + laneIndex + ' ] = ' + diff);
            return Math.sqrt((chunk.radius + diff) * glob.F_MUTHERFUCKER); // max: 0.445
        }
    }
    return 17;
}

function calculateBestSpeedForList(maxSpeedList, lengthList, acc) {

    maxSpeedList = maxSpeedList.reverse();
    lengthList = lengthList.reverse();

    var realMaxList = [],
        current, next, realCurrent, realNext = maxSpeedList[0], chunk, lastAngleChecked = false;

    realMaxList.push(realNext);

    for (var i = 0; i < maxSpeedList.length; i += 1) {
        chunk = gInit.track.pieces[gInit.track.pieces.length-i];
        if (typeof chunk !== 'undefined' && typeof chunk.radius !== 'undefined' && !lastAngleChecked) {
            glob.lastAngleIndex = gInit.track.pieces.length-i;
            console.log('LAST ANGLE INDEX: ', glob.lastAngleIndex);
            lastAngleChecked = true;
        }
        current = maxSpeedList[i];
        next = maxSpeedList[i + 1] || maxSpeedList[0];
        if (realNext < next) {
            realNext = realNext + (0.02 * lengthList[i]);
        } else {
            realNext = next;
        }
        realMaxList.push(realNext);
    }

    realMaxList.reverse();

    return realMaxList;
}

function round(num, abs) {
    var result = Math.round(num * 100) / 100;
    if (typeof abs === 'undefined') {
        abs = false;
    }
    return abs ? Math.abs(result) : result;
}

function accForSpeed(speed) {

}

function getCarIndex(dataList) {
    for (var i = 0; i < dataList.length; i += 1) {
        if (dataList[i].id.name === botName) {
            return i;
        }
    }

    throw new Error('Cannot get car Index ;(');
}

function pushTurbo() {
    send({"msgType": "turbo", "data": "NA ŁAZARSKIM REJONIE NIE JEST KOLOROWO!"});
    glob.isTurboAv = false;
}

function recalculateMaxSpeeds() {
    gInit.maxSpeedList = [];
    gInit.LengthList = [];

    /*for (var laneIndex = 0; laneIndex < numberOfLanes; laneIndex++) {
     gInit.maxSpeedList[laneIndex] = [];
     gInit.LengthList[laneIndex] = [];
     }*/

    for (var i = 0; i < gInit.track.pieces.length; i += 1) {
        gInit.chunk = gInit.track.pieces[i];

        for (var laneIndex = 0; laneIndex < numberOfLanes; laneIndex++) {
            gInit.LengthList[laneIndex] = gInit.LengthList[laneIndex] || [];
            gInit.maxSpeedList[laneIndex] = gInit.maxSpeedList[laneIndex] || [];

            gInit.LengthList[laneIndex].push(calculateDistanceForChunk(gInit.chunk, laneIndex));
            gInit.maxSpeedList[laneIndex].push(maxSpeedForChunk(gInit.chunk, laneIndex));
        }
    }

    for (var laneIndex = 0; laneIndex < numberOfLanes; laneIndex++) {
        glob.calculatedMaxSpeeds[laneIndex] = calculateBestSpeedForList(gInit.maxSpeedList[laneIndex], gInit.LengthList[laneIndex]);

        console.log('laneIndex ' + laneIndex);
        console.log(glob.calculatedMaxSpeeds[laneIndex]);
    }
}

function turboRecognition (tick, speed) {

}

function someCarBeforeMe() {

}

glob.lastSpeed = 0;
glob.lastDistance = 0;
glob.currentThrotle = 1;
glob.helcounter = 0;
glob.oncea = true;
glob.onceb = true;
glob.isTurboAv = false;
glob.distance = 0;
glob.shortestLaneBetweenSwitchesIndex = 0;
glob.laneChanged = false;
glob.calculatedMaxSpeeds = [];
glob.onSwitch = false;
glob.initCurrentLaneIndex = true;
glob.currentLaneIndex = 0;
glob.beforeFirstAngle = true;
glob.maxAngle = 0;
glob.lastLap = 0;
glob.carMass = 0;
glob.turboRec = {
    isReadyToCalculate: false,
    calcStartedInTick: 0,
    tickIndex: 0,
    V: []
};
glob.laneIndexes = [];

glob.F_MUTHERFUCKER = 0.425;

jsonStream.on('data', function (data) {
    if (data.msgType === 'carPositions') {
        console.log('====================================');

        var cPosition = {};
        cPosition.data = data.data[getCarIndex(data.data)];
        cPosition.piecePosition = cPosition.data.piecePosition;
        cPosition.inPieceDistance = cPosition.piecePosition.inPieceDistance;
        cPosition.lap = cPosition.piecePosition.lap;
        cPosition.speedBigFloat = (-1) * (glob.lastDistance - cPosition.inPieceDistance);
        cPosition.angleDiff = cPosition.data.angle - glob.lastAngle;
        cPosition.ABSangleDiff = Math.abs(cPosition.angleDiff);
        cPosition.ABSangle = Math.abs(cPosition.data.angle);
        cPosition.distance = 0;
        cPosition.chunk = [];
        cPosition.chunk[0] = gInit.track.pieces[cPosition.piecePosition.pieceIndex];
        cPosition.chunk[1] = gInit.track.pieces[cPosition.piecePosition.pieceIndex +1] || {};
        cPosition.chunk[2] = gInit.track.pieces[cPosition.piecePosition.pieceIndex +2] || {};

        var index;

        if (glob.initCurrentLaneIndex) {
            glob.currentLaneIndex = cPosition.piecePosition.lane.startLaneIndex;
            glob.initCurrentLaneIndex = false;
        }

        cPosition.chunkDistanceLeft = gInit.LengthList[glob.currentLaneIndex][cPosition.piecePosition.pieceIndex] - round(cPosition.inPieceDistance);

        glob.maxAngle = cPosition.ABSangle > glob.maxAngle ? cPosition.ABSangle : glob.maxAngle;
        glob.shortestLaneBetweenSwitchesIndex = getOnShortestLaneBetweenSwitches(gInit.track, cPosition.piecePosition.pieceIndex);

        if (typeof glob.turboRec.m !== 'undefined' && typeof glob.turboRec.k !== 'undefined') {
            glob.F_MUTHERFUCKER = glob.turboRec.m * glob.turboRec.k * 0.9;
        }

        // calculate distance
        // zmiana chunka
        if (glob.lastChunkDistance > cPosition.inPieceDistance) {
            glob.laneIndexes.push(glob.currentLaneIndex);
        }
        console.log('aaa ' + cPosition.piecePosition.pieceIndex);
        for (var i = 0; i < cPosition.piecePosition.pieceIndex; i++) {
            index = gInit.LengthList[0].length - 1 - i;

            console.log('index', index);
            console.log('distances', i, glob.laneIndexes[i],  gInit.LengthList[glob.laneIndexes[i]][index]);

            cPosition.distance += Math.floor(gInit.LengthList[glob.laneIndexes[i]][index]);
        }
        console.log('bbb');
        cPosition.distance += cPosition.inPieceDistance;
        cPosition.speed = round(glob.lastDistance - cPosition.distance, true);
        cPosition.speedBigFloat = (-1) * (glob.lastDistance - cPosition.distance);

        if (cPosition.data.angle > 0) {
            cPosition.anglePlus = true;
        } else if (cPosition.data.angle < 0) {
            cPosition.anglePlus = false;
        } else {
        }

        if (cPosition.angleDiff > 0) {
            cPosition.diffPlus = true;
        } else if (cPosition.angleDiff < 0) {
            cPosition.diffPlus = false;
        } else {
        }

        // Lane changing
        if (!glob.laneChanged && glob.shortestLaneBetweenSwitchesIndex !== glob.currentLaneIndex) {
            changeLane(glob.shortestLaneBetweenSwitchesIndex);
        }

        if (glob.laneChanged && typeof gInit.track.pieces[cPosition.piecePosition.pieceIndex].switch !== 'undefined') {
            glob.onSwitch = true;
        }

        if (glob.laneChanged && glob.onSwitch && typeof gInit.track.pieces[cPosition.piecePosition.pieceIndex].switch === 'undefined') {
            glob.currentLaneIndex = glob.sendLaneIndex;
            glob.onSwitch = false;
            glob.laneChanged = false;
        }

        // Common steering
        if (glob.beforeFirstAngle) {
            if (cPosition.speed > glob.calculatedMaxSpeeds[glob.currentLaneIndex][cPosition.piecePosition.pieceIndex + 1]-0.5) {
                glob.currentThrotle = 0;
                if (glob.calculatedMaxSpeeds[glob.currentLaneIndex][cPosition.piecePosition.pieceIndex + 1] < glob.calculatedMaxSpeeds[glob.currentLaneIndex][cPosition.piecePosition.pieceIndex + 2]) {
                    console.log('diifPlus', cPosition.diffPlus !== cPosition.anglePlus);
                    if (cPosition.diffPlus !== cPosition.anglePlus) {
                        glob.currentThrotle = 1;
                    }
                }
            } else {
                glob.currentThrotle = 1;
            }
        } else {
            if (cPosition.speed > glob.calculatedMaxSpeeds[glob.currentLaneIndex][cPosition.piecePosition.pieceIndex + 1]) {
                glob.currentThrotle = 0.2;
                // jesli roznica w predkosci jest mala to nie zwalniaj tak gwaltownie
                if (cPosition.speed - glob.calculatedMaxSpeeds[glob.currentLaneIndex][cPosition.piecePosition.pieceIndex + 1] > 0.5) {
                    glob.currentThrotle = 0;
                }
                if (glob.calculatedMaxSpeeds[glob.currentLaneIndex][cPosition.piecePosition.pieceIndex + 1] < glob.calculatedMaxSpeeds[glob.currentLaneIndex][cPosition.piecePosition.pieceIndex + 2]) {
                    console.log('diifPlus', cPosition.diffPlus !== cPosition.anglePlus);
                    if (cPosition.diffPlus !== cPosition.anglePlus) {
                        glob.currentThrotle = 1;
                    }
                }
            } else {
                glob.currentThrotle = 1;

                // nastepny zakret ma wiekszy kąt niż bieżący, trzeba zwolnić
                if(typeof cPosition.chunk[1].radius !=='undefined' && typeof cPosition.chunk[0].radius !== 'undefined') {
                    if (cPosition.chunk[1].radius > cPosition.chunk[0].radius) {
                        if (cPosition.chunkDistanceLeft < gInit.LengthList[glob.currentLaneIndex][cPosition.piecePosition.pieceIndex] / 3) {
                            glob.currentThrotle = 0;
                        }
                    }
                }
            }
        }

        if (cPosition.lap === 0 && typeof gInit.track.pieces[cPosition.piecePosition.pieceIndex].angle !== 'undefined' && typeof gInit.track.pieces[cPosition.piecePosition.pieceIndex+1].angle === 'undefined') {
            console.log('b');
            glob.beforeFirstAngle = false;
        }

        // Resetowanie beforeFirstAngle
        if (cPosition.lap !== glob.lastLap) {
            glob.beforeFirstAngle = true;
        }


        if (cPosition.ABSangle >= 57) { // awaryjne hamowanie
            glob.currentThrottle = 0;
        }

        // Avoidowanie schujowionego dystansu (skoki dystansu powodują skoki speeda)
        if (cPosition.speed > 20) {
            glob.currentThrottle = 0.6;
        }

        console.log("shortestLaneBetweenSwitchesIndex", glob.shortestLaneBetweenSwitchesIndex);
        console.log("currentLaneIndex", glob.currentLaneIndex);
        console.log('tr', glob.currentThrotle);
        console.log('acc', round(glob.lastSpeed - cPosition.speed, true));
        console.log('speed', cPosition.speed);
        console.log('maxSpeed lane 0', glob.calculatedMaxSpeeds[0][cPosition.piecePosition.pieceIndex + 1]);
        console.log('maxSpeed lane 1', glob.calculatedMaxSpeeds[1][cPosition.piecePosition.pieceIndex + 1]);
        console.log('maxSpeed ACTUAL', glob.calculatedMaxSpeeds[glob.currentLaneIndex][cPosition.piecePosition.pieceIndex + 1]);
        console.log('diff', cPosition.ABSangleDiff);
        console.log('angle', round(cPosition.data.angle));
        console.log('maxAngle', round(glob.maxAngle));
        console.log('MUTHERFUCKER', glob.F_MUTHERFUCKER);
        console.log('pieceIndex', cPosition.piecePosition.pieceIndex);
        console.log('turboIndex', glob.lastAngleIndex+1);
        console.log('turboAv', glob.isTurboAv, 'duration:', glob.turboDuration || 0);
        console.log('inpiece distance', round(cPosition.inPieceDistance));
        console.log('wholeDistance', round(cPosition.distance));
        console.log('lap ' + (cPosition.lap+1) + '/' + maxLaps);

        if (!glob.turboRec.isReadyToCalculate) {
            if (glob.lastSpeed !== cPosition.speed && glob.lastSpeed === 0) {
                glob.turboRec.V[0] = glob.lastSpeed;
                glob.turboRec.V[glob.turboRec.tickIndex + 1] = cPosition.speedBigFloat;

                glob.turboRec.isReadyToCalculate = true;
                glob.turboRec.calcStartedInTick = data.gameTick;
                glob.turboRec.tickIndex++;
            }
        } else if(glob.turboRec.tickIndex < 3) {
            glob.turboRec.V[glob.turboRec.tickIndex + 1] = cPosition.speedBigFloat;
            glob.turboRec.tickIndex++;

            console.log('V', glob.turboRec.V[glob.turboRec.calcStartedInTick - data.gameTick + 1]);
        } else if(glob.turboRec.tickIndex === 3) {
            // k = ( v(1) - ( v(2) - v(1) ) ) / v(1)2 * h;      h - trottle
            glob.turboRec.k = ( glob.turboRec.V[1] - ( glob.turboRec.V[2] - glob.turboRec.V[1] ) ) / Math.pow(glob.turboRec.V[1], 2) * 1;

            // m = 1.0 / ( ln( ( v(3) - ( h / k ) ) / ( v(2) - ( h / k ) ) ) / ( -k ) )
            glob.turboRec.m = 1 / ( Math.log( ( glob.turboRec.V[3] - ( 1 / glob.turboRec.k ) ) / ( glob.turboRec.V[2] - ( 1 / glob.turboRec.k ) ) ) / ( -glob.turboRec.k ) );
        }

/*
        // Prędkość w następnych po t tickach - future V
        // v(t) = (v(0) - (h/k) ) * e^ ( ( - k * t ) / m ) + ( h/k )
        glob.turboRec.fV = (cPosition.speedBigFloat - (glob.currentThrotle/glob.turboRec.k) ) * Math.exp( ( - glob.turboRec.k * 5 ) / glob.turboRec.m ) + ( glob.currentThrotle/glob.turboRec.k );

        // Za ile tików zostanie osiagnieta predkosc 2.2?
        // t = ( ln ( (v - ( h/k ) )/(v(0) - ( h/k ) ) ) * m ) / ( -k )
        glob.turboRec.t = ( Math.log( (2.2 - ( glob.currentThrotle/glob.turboRec.k ) )/(cPosition.speedBigFloat - ( glob.currentThrotle/glob.turboRec.k ) ) ) * glob.turboRec.m ) / ( -glob.turboRec.k )

        // d(t) = ( m/k ) * ( v(0) - ( h/k ) ) * ( 1.0 - e^ ( ( -k*t ) / m ) ) + ( h/k ) * t + d(0)
        // d(0): Your current position, 0.0 if you want a relative distance
        glob.turboRec.d = ( glob.turboRec.m/glob.turboRec.k ) * ( cPosition.speedBigFloat - ( glob.currentThrotle/glob.turboRec.k ) ) * ( 1.0 - Math.exp ( ( -glob.turboRec.k*5 ) / glob.turboRec.m ) ) + ( glob.currentThrotle/glob.turboRec.k ) * 5;
*/

        console.log('k', glob.turboRec.k);
        console.log('m', glob.turboRec.m);
//        console.log('d', glob.turboRec.d);

        glob.lastAngle = cPosition.data.angle;
        glob.lastDistance = cPosition.distance;
        glob.lastChunkDistance = cPosition.inPieceDistance;
        glob.lastSpeed = cPosition.speed;
        glob.lastLap = cPosition.lap;
        glob.lastPieceIndex = cPosition.piecePosition.pieceIndex;

        /*
            TURBO TURBO TURBO TURBO TURBO TURBO TURBO TURBO
            TURBO TURBO TURBO TURBO TURBO TURBO TURBO TURBO
            TURBO TURBO TURBO TURBO TURBO TURBO TURBO TURBO
         */
        var speedDiff = glob.calculatedMaxSpeeds[glob.currentLaneIndex][cPosition.piecePosition.pieceIndex + 1] - cPosition.speed;
        console.log('speedDiff', speedDiff);

        if (glob.isTurboAv) {
            // Tylko gdy to nie jest ostatni lap, bo chcemy użyć jeszcze na koniec
            if (cPosition.lap+1 !== maxLaps) {
                glob.turboRec.d = ( glob.turboRec.m/glob.turboRec.k ) * ( cPosition.speedBigFloat - ( glob.currentThrotle*glob.turboFactor/glob.turboRec.k ) ) * ( 1.0 - Math.exp ( ( -glob.turboRec.k*glob.turboDurationTicks ) / glob.turboRec.m ) ) + ( glob.currentThrotle*glob.turboFactor/glob.turboRec.k ) * glob.turboDurationTicks;
                glob.turboRec.fV = (cPosition.speedBigFloat - (glob.currentThrotle*glob.turboFactor/glob.turboRec.k) ) * Math.exp( ( - glob.turboRec.k * glob.turboDurationTicks ) / glob.turboRec.m ) + ( glob.currentThrotle*glob.turboFactor/glob.turboRec.k );

                console.log('==== TURBO AV');
                console.log('glob.turboFactor', glob.turboFactor);
                console.log('glob.turboDurationTicks', glob.turboDurationTicks);
                console.log('d', glob.turboRec.d);      // jeśli użyjesz turbo w tym tiku to przejedziesz na nim tyle metrów
                console.log('dd', calculateDistanceToNextAngle(cPosition));    // i będziesz miał taką prędkość na końcu turbo
                console.log('fV', glob.turboRec.fV);    // i będziesz miał taką prędkość na końcu turbo
                console.log('==== TURBO AV');

                if (calculateDistanceToNextAngle(cPosition) > glob.turboRec.d) {
                    pushTurbo();
                    return;
                }
/*
                // Gdy można jechać fchuj szybciej w następnym chunku to pod koniec bieżącego odpal turbo
                if ( speedDiff > 7 && cPosition.speed < 6) { // gdy speed większy od 6 to kaplica
                    if(cPosition.chunkDistanceLeft < 20) {
                        pushTurbo();
                        return;
                    }
                }

//                 Dosyć śliskie bo nietestowane
                if ((speedDiff > 4 && cPosition.speed < 3) || speedDiff > 9) {
                    pushTurbo();
                    return;
                }
                if (glob.turboDuration <= 300 && speedDiff > 6) {
                    pushTurbo();
                    return;
                }*/
            }
            // turbo na koniec lapa
            if (glob.lastAngleIndex+1 === cPosition.piecePosition.pieceIndex && cPosition.lap+1 === maxLaps) {
                pushTurbo();
                return; // 1 send na tick!
            }
        }

        pushThrottle(glob.currentThrotle, data.gameTick);
        console.log('==================================== ' + data.gameTick);
    } else {
        if (data.msgType === 'join') {
            console.log('Joined')
        } else if (data.msgType === 'turboAvailable') {
            glob.isTurboAv = true;
            glob.turboDuration = data.data.turboDurationMilliseconds;
            glob.turboDurationTicks = data.data.turboDurationTicks;
            glob.turboFactor = data.data.turboFactor;
            console.log('********************************************* ****** TURBO!!!!!!!', data.data.turboDurationMilliseconds);
        } else if (data.msgType === 'gameStart') {
            glob.laneChanged = false;

            console.log(data);
            console.log('Race started');
        } else if (data.msgType === 'gameEnd') {
            console.log('Race ended');
            console.log('BestLap', data.data.bestLaps[0].result.millis / 1000);
        } else if (data.msgType === 'gameInit') {
            console.log(data.data.race.track);

            maxLaps = data.data.race.raceSession.laps;
            numberOfLanes = data.data.race.track.lanes.length;

            gInit.track = data.data.race.track;

            recalculateMaxSpeeds();

        }

        send({
            msgType: "ping",
            data: {}
        });
    }
});

jsonStream.on('error', function () {
    return console.log("disconnected");
});